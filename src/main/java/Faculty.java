import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

public class Faculty {

    private Integer fID;
    private String name;
    private String description;
    private String foundingDate;

    MongoClient mongoClient = new MongoClient("localhost", 27017);
    MongoDatabase database = mongoClient.getDatabase("StudentDB");
    MongoCollection collection = database.getCollection("Faculties");

    public Faculty(String name, String description, String foundingDate) {
        this.name = name;
        this.description = description;
        this.foundingDate = foundingDate;
    }

    public void addFaculty (Faculty faculty){
        Document document = new Document("fID",fID);
        document.append("Name",name);
        document.append("Description", description);
        document.append("Founding Date",foundingDate);

        collection.insertOne(faculty);
    }

    public Document foundFaculty (Integer fID){
        return(Document) collection.find(new Document("fID",fID)).first();
    }

    public Document foundFaculty (String facultyName){
        return(Document) collection.find(new Document("Name",facultyName)).first();
    }

    public void updateFaculty (Integer fID,String nameOfValue, String value){
        Bson update = new Document(nameOfValue,value);
        Bson updateoperation = new Document("&set", update);
        collection.updateOne(foundFaculty(fID),updateoperation);
    }

    public void updateFaculty (String facultyName,String nameOfValue, String value){
        Bson update = new Document(nameOfValue,value);
        Bson updateoperation = new Document("&set", update);
        collection.updateOne(foundFaculty(facultyName),updateoperation);
    }

    public void deleteStudent (Integer fID){
        collection.deleteOne(foundFaculty(fID));
    }

    public void deleteStudent (String facultyName){
        collection.deleteOne(foundFaculty(facultyName));
    }

    public void show(){
        FindIterable<Document> findIterable = collection.find();
        MongoCursor<Document> mongoCursor = findIterable.iterator();
        try {
            while (mongoCursor.hasNext()){
                System.out.println(mongoCursor.next().toJson());
            }
        } finally {
            mongoCursor.close();
        }
    }

}
