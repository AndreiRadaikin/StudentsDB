import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;

import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.Arrays;


public class Group  {

    private Integer gID;
    private String groupName;
    private Student headman;
    private Faculty faculty;
    private MongoCollection<Student> students;
    private MongoCollection<Lecturer> lecturers;


    MongoClient mongoClient = new MongoClient("localhost", 27017);
    MongoDatabase database = mongoClient.getDatabase("StudentDB");
    MongoCollection collection = database.getCollection("Group");

    public Group(String groupName, Student headman, Faculty faculty) {
        this.groupName = groupName;
        this.headman = headman;
        this.faculty = faculty;
    }

    public void addGroup (Group group){
        Document document = new Document("gID",gID);
        document.append("Group name", groupName);
        document.append("Headman",headman);
        document.append("Faculty",faculty);

        collection.insertOne(group);
    }

    public Document foundGroup (Integer gID){
        return(Document) collection.find(new Document("gID",gID)).first();
    }

    public Document foundGroup (String groupName){
        return(Document) collection.find(new Document("Group name",groupName)).first();
    }

    public void updateGroup (Integer gID,String nameOfValue, String value){
        Bson update = new Document(nameOfValue,value);
        Bson updateoperation = new Document("&set", update);
        collection.updateOne(foundGroup(gID),updateoperation);
    }

    public void updateGroup (String GroupName,String nameOfValue, String value){
        Bson update = new Document(nameOfValue,value);
        Bson updateoperation = new Document("&set", update);
        collection.updateOne(foundGroup(GroupName),updateoperation);
    }

    public void deleteStudent (Integer gID){
        collection.deleteOne(foundGroup(gID));
    }

    public void deleteStudent (String GroupName){
        collection.deleteOne(foundGroup(GroupName));
    }

    public void show(){
        FindIterable<Document> findIterable = collection.find();
        MongoCursor<Document> mongoCursor = findIterable.iterator();
        try {
            while (mongoCursor.hasNext()){
                System.out.println(mongoCursor.next().toJson());
            }
        } finally {
            mongoCursor.close();
        }
    }

    public void addStudentInTheGroup (Student student){
        students.insertOne(student);
    }

    public void deleteStudentFromTheGroupBySurname (String surname){
        students.findOneAndDelete(Filters.eq("Surname",surname));
    }

    public void addLecturerInTheGroup (Lecturer lecturer){
        lecturers.insertOne(lecturer);
    }

    public void deleteLecturerFromTheGroupBySurname (String surname){
        lecturers.findOneAndDelete(Filters.eq("Surname",surname));
    }

    public void showAllStudentFromTheGroup(){
        FindIterable<Student> findIterable = students.find();
        MongoCursor<Student> Cursor = findIterable.iterator();
        try {
            while (Cursor.hasNext()){
                System.out.println(Cursor.next());
            }
        } finally {
            Cursor.close();
        }
    }

    public void showTopThreeStudentsByAvgMark(){

        Block<Student> printBlock = document -> System.out.println(document);


        students.aggregate(
                Arrays.asList(Aggregates.sort(Sorts.descending()), Aggregates.limit(3))
        ).forEach(printBlock);
    }


}
