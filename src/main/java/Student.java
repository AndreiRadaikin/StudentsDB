import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;

public class Student extends Human {

    private Integer sID;
    private String educationForm;
    private Integer avgMark;

    MongoClient mongoClient = new MongoClient("localhost", 27017);
    MongoDatabase database = mongoClient.getDatabase("StudentDB");
    MongoCollection collection = database.getCollection("Student");

    public Student(String name, String surname, String patronymic, Integer age,
                   String mail, String birthday, Integer sID, String educationForm,
                   Integer avgMark) {
        super(name, surname, patronymic, age, mail, birthday);
        this.sID = sID;
        this.educationForm = educationForm;
        this.avgMark = avgMark;
    }

   public void add (Student student){
       Document document = new Document("sID",sID);
       document.append("Surname", getSurname());
       document.append("Name", getName());
       document.append("Patronymic",getPatronymic());
       document.append("Age",getAge());
       document.append("Mail", getMail());
       document.append("Birthday", getBirthday());
       document.append("Education Form",educationForm);
       document.append("Average mark", avgMark);

       collection.insertOne(student);

   }

   public Document found (Integer sID){
       return(Document) collection.find(new Document("sID",sID)).first();
   }

   public Document found (String surname){
       return(Document) collection.find(new Document("Surname",getSurname())).first();
   }

   public void update (Integer sID,String nameOfValue, String value){
       Bson update = new Document(nameOfValue,value);
       Bson updateoperation = new Document("&set", update);
       collection.updateOne(found(sID),updateoperation);
   }

   public void update (String surname,String nameOfValue, String value){
       Bson update = new Document(nameOfValue,value);
       Bson updateoperation = new Document("&set", update);
       collection.updateOne(found(surname),updateoperation);
   }

   public void delete (Integer sID){
        collection.deleteOne(found(sID));
   }

   public void delete (String surname){
        collection.deleteOne(found(surname));
   }

   public void show(){
       FindIterable<Document> findIterable = collection.find();
       MongoCursor<Document> mongoCursor = findIterable.iterator();
       try {
           while (mongoCursor.hasNext()){
               System.out.println(mongoCursor.next().toJson());
           }
       } finally {
           mongoCursor.close();
       }
   }


}
