import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

public class Lecturer extends Human {

    private Integer lID;
    private String specialization;
    private String degree;

    MongoClient mongoClient = new MongoClient("localhost", 27017);
    MongoDatabase database = mongoClient.getDatabase("StudentDB");

    MongoCollection collection = database.getCollection("Lecturer");

    public Lecturer(String name, String surname, String patronymic, Integer age,
                    String mail, String birthday, Integer lID, String specialization,
                    String degree) {
        super(name, surname, patronymic, age, mail, birthday);
        this.lID = lID;
        this.specialization = specialization;
        this.degree = degree;
    }

    public void addLecturer (Lecturer lecturer){
        Document document = new Document("sID",lID);
        document.append("Surname", getSurname());
        document.append("Name", getName());
        document.append("Patronymic",getPatronymic());
        document.append("Age",getAge());
        document.append("Mail", getMail());
        document.append("Birthday", getBirthday());
        document.append("Specialization", specialization);
        document.append("Degree", degree);

        collection.insertOne(lecturer);
    }

    public Document foundLecturer (Integer lID){
        return (Document) collection.find(new Document("lID",lID)).first();
    }
    public Document foundLecturer (String surname){
        return (Document) collection.find(new Document("Surname",getSurname())).first();
    }

    public void updateLecturer (Integer sID,String nameOfValue, String value){
        Bson update = new Document(nameOfValue,value);
        Bson updateoperation = new Document("&set", update);
        collection.updateOne(foundLecturer(sID),updateoperation);
    }

    public void updateLecturer (String surname,String nameOfValue, String value){
        Bson update = new Document(nameOfValue,value);
        Bson updateoperation = new Document("&set", update);
        collection.updateOne(foundLecturer(surname),updateoperation);
    }

    public void deleteLecturer (Integer sID){
        collection.deleteOne(foundLecturer(sID));
    }

    public void deleteLecturer (String surname){
        collection.deleteOne(foundLecturer(surname));
    }

    public void showAllStudents(){
        FindIterable<Document> findIterable = collection.find();
        MongoCursor<Document> mongoCursor = findIterable.iterator();
        try {
            while (mongoCursor.hasNext()){
                System.out.println(mongoCursor.next().toJson());
            }
        } finally {
            mongoCursor.close();
        }
    }


}
