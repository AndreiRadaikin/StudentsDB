public interface IFaculty {
    Integer getFID();

    String  getDescription();
    Boolean setDescription(String d);

    String getDate();
    Boolean setDate(String date);
}
