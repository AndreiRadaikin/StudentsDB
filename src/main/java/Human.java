import javax.swing.text.Document;

public class Human {

    private String name;
    private String surname;
    private String patronymic;
    private Integer age;
    private String mail;
    private String birthday;

    public Human(String name, String surname, String patronymic, Integer age, String mail, String birthday) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
        this.mail = mail;
        this.birthday = birthday;
    }



    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Integer getAge() {
        return age;
    }

    public String getMail() {
        return mail;
    }

    public String getBirthday() {
        return birthday;
    }
}
