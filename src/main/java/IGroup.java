public interface IGroup {
    Integer getGID();

    String getGroupName();
    Boolean setGroupName(String name);

    Human getHeadman();
    Boolean setHeadman(Human s);

    Faculty getFaculty();
    Boolean setfaculty(Faculty f);
}
