public interface IHuman {
    Integer getHumanID(Integer ID);

    String getName();
    Boolean setName(String n);

    String getSurname();
    Boolean setSurname(String s);

    String getPatronymic();
    Boolean setPatronymic(String p);

    Integer getAge();
    Boolean setAge(Integer a);

    String getMail();
    Boolean setMail(String m);

    String getBirthday();
    Boolean setBirthday(String b);
}
